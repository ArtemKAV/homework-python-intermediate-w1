'''
Задание 2
Создать класс Point2D. Координаты точки задаются 2 параметрами - координатами x, y на плоскости.
Реализовать метод distance который принимает экземпляр класса Point2D и рассчитывает расстояние между 2мя точками на
плоскости.
Создать защищенный атрибут класса - счетчик созданных экземпляров класса.
Чтение количества экземпляров реализовать через метод getter.
Создать класс Point3D, который является наследником класса Point2D. Координаты точки задаются
3 параметрами - координатами x, y, z в пространстве.
Переопределить конструктор с помощью super().
Переопределить метод distance для определения расстояния между 2-мя точками в пространстве.
'''


# Создать класс Point2D

class Point2D:
    # Создать защищенный атрибут класса - счетчик созданных экземпляров класса.
    __count_points = 0

    @classmethod
    def getter(cls):
        return cls.__count_points

    def __init__(self, x, y):
        Point2D.__count_points += 1
        self.x = x
        self.y = y

    # Реализовать метод distance который принимает экземпляр класса Point2D и рассчитывает
    # расстояние между 2мя точками на плоскости.
    def distance(self, other_point):
        return ((other_point.x - self.x) ** 2 + (other_point.y - self.y) ** 2) ** 0.5


class Point3D(Point2D):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def distance(self, other_point):
        return ((other_point.x - self.x) ** 2 + (other_point.y - self.y) ** 2 + 
                (other_point.z ** 2 - self.z ** 2)) ** 0.5


point1 = Point2D(1, 2)
print(point1.getter())
point2 = Point2D(2, 4)
print(point2.getter())

print(point1.distance(point2))

point3 = Point3D(1, 2, 3)
point4 = Point3D(4, 5, 6)

print(point4.getter())
