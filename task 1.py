'''
Задание 1
Создать класс дробь(Fraction), конструктор которого принимает целые числа (num, den  -  числитель(numerator),
знаменатель(denominator) ).
Выполнить
Атрибуты числитель и знаменатель в классе сделать приватными. Доступ к атрибутам реализовать через свойства.
Переопределить методы __sub__, __add__, __mull__, __truediv__ для того, чтобы можно было выполнять соответствующие
математические действия  с объектами класса дробь.
(Вычитание, сложение, умножение и деление).
В миксине реализовать статические методы, для этих же операций(add, sub, mull, div).
Добавить в класс миксин.
Создать classmethod который из строки типа 'числитель/знаменатель' возвращает объект класса дробь.
Переопределить метод __str__, который при выводе объекта на печать будет выводить строку вида num / den.
Создать объекты класса дробь.
Выполнить все реализованные методы.
'''


# В миксине реализовать статические методы, для этих же операций(add, sub, mull, div). Добавить в класс миксин.


class Mixin_for_fraction:
    @staticmethod
    def add(first_fraction, second_fraction):
        return Fraction(first_fraction.get_num * second_fraction.get_den + first_fraction.get_den * second_fraction.get_num,
                        first_fraction.get_den * second_fraction.get_den)

    @staticmethod
    def sub(first_fraction, second_fraction):
        return Fraction(first_fraction.get_num * second_fraction.get_den - first_fraction.get_den * second_fraction.get_num,
                        first_fraction.get_den * second_fraction.get_den)

    @staticmethod
    def mull(first_fraction, second_fraction):
        return Fraction(first_fraction.get_num * second_fraction.get_num, first_fraction.get_den * second_fraction.get_den)

    @staticmethod
    def div(first_fraction, second_fraction):
        return Fraction(first_fraction.get_num * second_fraction.get_den, first_fraction.get_den * second_fraction.get_num)


# Создать класс дробь(Fraction), конструктор которого принимает целые числа (num, den)

class Fraction(Mixin_for_fraction):
    # Атрибуты числитель и знаменатель в классе сделать приватными.
    def __init__(self, num, den):
        # Сокращение дроби
        flag = 1
        while flag:
            end_point = min(num, den)
            flag = 0
            if end_point > 1:
                for i in range(2, end_point + 1):
                    if num % i == 0 and den % i == 0:
                        num = int(num / i)
                        den = int(den / i)
                        flag = 1
        self.__num = num
        self.__den = den

    # Переопределить метод __str__
    def __str__(self):
        return f'{self.get_num}/{self.get_den}'

    # @staticmethod                     # Можно использовать статический метод в методах тогоже класса где он объявлен?
    # def reduce_fraction(a, b):
    #     flag = 1
    #     while flag:
    #         end_point = min(a, b)
    #         flag = 0
    #         if end_point > 1:
    #             for i in range(2, end_point + 1):
    #                 if a % i == 0 and b % i == 0:
    #                     a = int(a / i)
    #                     b = int(b / i)
    #                     flag = 1
    #
    #     return (a, b)

    # Доступ к атрибутам реализовать через свойства.
    @property
    def get_num(self):
        return self.__num

    @property
    def get_den(self):
        return self.__den

    # Переопределить методы __sub__, __add__, __mull__, __truediv__

    def __sub__(self, fraction):
        num = self.get_num * fraction.get_den - self.get_den * fraction.get_num
        den = self.get_den * fraction.get_den

        return Fraction(num, den)

    def __add__(self, fraction):
        num = self.get_num * fraction.get_den + self.get_den * fraction.get_num
        den = self.get_den * fraction.get_den

        return Fraction(num, den)

    def __mul__(self, fraction):
        num = self.get_num * fraction.get_num
        den = self.get_den * fraction.get_den

        return Fraction(num, den)

    def __truediv__(self, fraction):
        num = self.get_num * fraction.get_den
        den = self.get_den * fraction.get_num

        return Fraction(num, den)

    #   Создать classmethod который из строки типа 'числитель/знаменатель' возвращает объект класса дробь.
    @classmethod
    def fraction_from_string(cls, fraction_in_string):
        num, den = [int(i.strip()) for i in fraction_in_string.split('/')]
        return cls(num, den)


# В миксине реализовать статические методы, для этих же операций(add, sub, mull, div). Добавить в класс миксин.


class Mixin_for_fraction:
    def add(self, second_fraction):
        return Fraction(self.get_num * second_fraction.get_den + self.get_den * second_fraction.get_num,
                        self.get_den * second_fraction.get_den)


class my_fraction(Mixin_for_fraction, Fraction):
    pass


f1 = my_fraction(1, 4)
f2 = my_fraction.fraction_from_string('1/8')

print('sub with __sub__', f1 - f2)
print('sub with mixin', f1.sub(f2), end='\n\n')
print('add with __add__', f1 + f2)
print('add with mixin', f1.add(f2), end='\n\n')
print('mull with __mul__', f1 * f2)
print('mull with mixin', f1.mull(f2), end='\n\n')
print('div with __truediv__', f1 / f2)
print('div with mixin', f1.div(f2))

